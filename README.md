A daily build repository of *completely offline* Arch Linux + ZFS + Plasma ISOs.

[Release Page](https://kayg.org/public/archlinux/iso/kde/)

